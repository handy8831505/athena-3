import random
import time
from datetime import datetime
import sqlite3



def generate_fake_sensor_data():


    temperature_mean, temperature_std_dev = 22, 3
    humidity_mean, humidity_std_dev = 50, 10
    air_quality_mean, air_quality_std_dev = 50, 20

    temperature = round(random.gauss(temperature_mean, temperature_std_dev), 2)
    humidity    = round(random.gauss(humidity_mean, humidity_std_dev), 2)
    air_quality = max(0, 
                      min(100, 
                          round(random.gauss(air_quality_mean, air_quality_std_dev))))


    '''temperature = round(random.uniform(15, 30), 2)
    humidity = round(random.uniform(30, 70), 2)
    air_quality = random.randint(0, 100)
    '''

    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    return {
        "timestamp": timestamp,
        "temperature": temperature,
        "humidity": humidity,
        "air_quality": air_quality
    }


def main():

    conn = sqlite3.connect('airsensor_data.db')
    cursor = conn.cursor()


    cursor.execute('''
        CREATE TABLE IF NOT EXISTS air_sensor_data(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            timestamp DATETIME,
            temperature REAL,
            humidity REAL
        )
    ''')

    while True:

        fake_data = generate_fake_sensor_data()
        print(fake_data)

        ts   = fake_data['timestamp']
        temp = fake_data['temperature']
        rh   = fake_data['humidity']


        cursor = conn.cursor()
        cursor.execute('''
            INSERT INTO air_sensor_data (timestamp, temperature, humidity)
            VALUES (?, ?, ?)
        ''', (ts, temp, rh))
        conn.commit()

        time.sleep(60)


if __name__ == "__main__":
    main()
