import ssl
import argparse
import paho.mqtt.client as mqtt
import json
import sqlite3

class ProductionServerConfig(object):
    URL = 'mqtt.urbanblue.ch'
    PORT = 8883
    USERNAME = 'general'
    PASSWORD = '8R3W*q)&'
    TLS = True

class StagingServerConfig(object):
    URL = 'stagingmqtt.urbanblue.ch'
    PORT = 1883
    USERNAME = 'stagingAdmin'
    PASSWORD = 'asdrew#@!09'
    TLS = False 

class InhouseServerConfig(object):
    URL = '192.168.0.141'
    PORT = 1883
    USERNAME = 'BLANK'
    PASSWORD = 'BLANK'
    TLS = False 


class MQTTClient:
    def __init__(self, config):
        self.config = config
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        # tls
        if self.config.get('tls'):
            # Create an SSL/TLS context that ignores certificate verification errors.
            ssl_context = ssl.create_default_context()
            ssl_context.check_hostname = False
            ssl_context.verify_mode = ssl.CERT_NONE
            self.client.tls_set_context(context=ssl_context)
            
        # login
        self.client.username_pw_set(self.config.get('username'), self.config.get('password'))

        # topics
        self.sub_measurements_topic = '{}/measurements'.format(self.config.get('gateway'))
        self.sub_events_topic = '{}/events'.format(self.config.get('gateway'))
        self.sub_topics = ['AA017A0002/#' ]

        
        self.conn = sqlite3.connect('soil.db')
        self.cursor = self.conn.cursor()

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS soil(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                hub TEXT,
                timestamp DATETIME,
                temp REAL,
                rh REAL
            )
        ''')





    def connect(self):
        self.client.connect(self.config.get('url'), self.config.get('port'))


    def disconnect(self):
        self.client.disconnect()


    def loop_forever(self):
        self.client.loop_forever()


    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))

        for topic in self.sub_topics:
            self.client.subscribe(topic)
            print('Subscribed {}'.format(topic))


    def on_message(self, client, userdata, msg):
        try:
            print("[{}]: {}".format(msg.topic, str(msg.payload)))
            filename = '{}.log'.format(msg.topic)
            filename = filename.replace('/', '-')

            json_obj = json.loads(msg.payload)
            with open(filename, 'a') as fd:
                s = json.dumps(json_obj)
                s = s + '\n'
                fd.write(s)
                
            s = json.dumps(json_obj, indent=2)
            print(s)
            print('---')

            ts = json_obj['time']
            hub = json_obj['hub']
            if hub.startswith('09'):
                temp = json_obj['params'][0]['value']
                rh = json_obj['params'][1]['value']

                print('{}, {}, {}, {}\n'.format(hub, ts, temp, rh))

                self.cursor.execute('''
                    INSERT INTO soil (hub, timestamp, temp, rh)
                    VALUES (?, ?, ?, ?)
                ''', (hub, ts, temp, rh))
                self.conn.commit()


                print('------')

        except Exception as err:
            print(err)

    
def main():
    parser = argparse.ArgumentParser(description='MQTT client connection')
    parser.add_argument('-s', '--server', type=str, 
                        choices=['production', 'staging', 'inhouse'],
                        default='production')
    parser.add_argument('-g', '--gateway', type=str, default='AA01530029')
    args = parser.parse_args()

    print(f'SERVER: {args.server}')
    if args.server == 'production':
        server_config = ProductionServerConfig() 
    elif args.server == 'staging':
        server_config = StagingServerConfig() 
    elif args.server == 'inhouse':
        server_config = InhouseServerConfig() 

    config = {'url': server_config.URL,
              'port': server_config.PORT,
              'username': server_config.USERNAME,
              'password': server_config.PASSWORD,
              'tls': server_config.TLS,
              'gateway': args.gateway
             }

    print('config: {}'.format(config))
    mclient = MQTTClient(config)
    mclient.connect()
    try:
        while True:
            mclient.loop_forever()
    except KeyboardInterrupt:
        mclient.disconnect()
        print('MQTT client is disconnected.')


if __name__ == "__main__":
    main()
